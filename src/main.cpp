#include <iostream>
#include <boost/tuple/tuple.hpp>
extern "C" {
#include <quadmath.h>
}
#include "complex.h"
#include "CompManag.hpp"
#include <mutex>
#include <thread>
//#include "gnuplot-iostream.h"



using namespace CompManag;


make_description(label,
class MyCalculator:public ICalculator
    {
    private:
        class ParametrContainer:public BaseContainer<IParameter>
            {
            public:
                ParametrContainer(MyCalculator *in)
                    {
                    AddItem("gam",std::shared_ptr<IParameter>(new ParameterBase("gam",&in->gam)));
                    AddItem("eps",std::shared_ptr<IParameter>(new ParameterBase("eps",&in->epsm)));
                    AddItem("nt",std::shared_ptr<IParameter>(new ParameterBase("nt",&in->nt)));
                    AddItem("Q",std::shared_ptr<IParameter>(new ParameterBase("Q",&in->Q)));
                    AddItem("NoiseWidth",std::shared_ptr<IParameter>(new ParameterBase("NoiseWidth",&in->NW)));
                    AddItem("teta",std::shared_ptr<IParameter>(new ParameterBase("teta",&in->teta)));
                    AddItem("dt",std::shared_ptr<IParameter>(new ParameterBase("dt",&in->dt)));
                    AddItem("Nmax",std::shared_ptr<IParameter>(new ParameterBase("Nmax",&in->Nmax)));
                    AddItem("dN",std::shared_ptr<IParameter>(new ParameterBase("dN",&in->dN)));
                    }
            
                ~ParametrContainer(){};
            };
        
        class OutputOne:public IOutputData
            {
            private:
                MyCalculator * parent;
                unsigned int out_id;
                std::string name;
            public:
                OutputOne(const std::string &na,MyCalculator *p,unsigned int id):
                                        parent(p),out_id(id),name(na)
                    {
                    }
                virtual ~OutputOne(){};
                virtual std::string GetName(void)
                    {
                    return name;
                    }
                
                virtual unsigned int GetCount(void)
                    {
                    return parent->OutputNum;
                    }
                
                virtual std::list<unsigned int> GetDimensions(void)
                    {
                    std::list<unsigned int> tmp;
                    tmp.push_back(parent->OutputNum);
                    return tmp;
                    }
                virtual std::vector<double>& GetData(void)
                    {
                    return parent->output[out_id];
                    }
            };
        
        class OutputContainer:public BaseContainer<IOutputData>
            {
            public:    
                OutputContainer(MyCalculator *in)
                    {
                    AddItem("t",std::shared_ptr<IOutputData>
                    (new OutputOne("t",in,0)));
                    AddItem("I1",std::shared_ptr<IOutputData>
                    (new OutputOne("I1",in,1)));
                    AddItem("I2",std::shared_ptr<IOutputData>
                    (new OutputOne("I2",in,2)));
                    AddItem("ak1*a2_re",std::shared_ptr<IOutputData>
                    (new OutputOne("ak1*a2_re",in,3)));
                    AddItem("ak1*a2_im",std::shared_ptr<IOutputData>
                    (new OutputOne("ak1*a2_im",in,4)));
                    AddItem("alph",std::shared_ptr<IOutputData>
                    (new OutputOne("alph",in,5)));
                    AddItem("a1*a2_re",std::shared_ptr<IOutputData>
                    (new OutputOne("a1*a2_re",in,6)));
                    AddItem("a1*a2_im",std::shared_ptr<IOutputData>
                    (new OutputOne("a1*a2_im",in,7)));
                    AddItem("alph1",std::shared_ptr<IOutputData>
                    (new OutputOne("alph1",in,8)));
                    AddItem("En",std::shared_ptr<IOutputData>
                    (new OutputOne("En",in,9)));
                    }
                ~OutputContainer(){};
            };
        
        
        
        
        unsigned int OutputNum;
        Runge::RungeSolver<complex<double> > runge;
        Runge::RungeSolverInit<complex<double> > runge_params;
        
        
        double gam;
        
        double eps;
        double epsm;
        
        double Q;
        double nt;
        double D;
        double NW;
        double teta;
        double dt;
        double Nmax;
        double dN;
        
        
        //tmp
        __float128 Sigm;
        std::vector<__float128> Mat16;
        std::vector<__float128> Mat9;
        std::vector<__float128> Mat4;
        __float128 Det;
        
        ParametrContainer params_pointer;
        OutputContainer outputs;
    
        std::vector<std::vector<double> > output;
    
        __float128 Det4(void)
            {
            return Mat4[0]*Mat4[3]-Mat4[1]*Mat4[2];
            }
        
        
        __float128 Det9(void)
            {
            __float128 sum=0;
            __float128 koe=1;
            for(unsigned int i=0;i<3;i++)
                {
                unsigned int l=0;
                for(unsigned int j=0;j<3;j++)
                    for(unsigned int k=1;k<3;k++)
                        if(j!=i)
                            {
                            Mat4[l]=Mat9[3*k+j];
                            l++;
                            }
                sum+=Mat9[i]*koe*Det4();
                koe*=-1;
                }
            return sum;
            }
        
        __float128 Det16(void)
            {
            __float128 sum=0;
            __float128 koe=1;
            for(unsigned int i=0;i<4;i++)
                {
                unsigned int l=0;
                for(unsigned int j=0;j<4;j++)
                    for(unsigned int k=1;k<4;k++)
                        if(j!=i)
                            {
                            Mat9[l]=Mat16[4*k+j];
                            l++;
                            }
                sum+=Mat16[i]*koe*Det9();
                koe*=-1;
                }
            return sum;
            }
        
        
        void Init(complex<double> * in)
            {
            in[0]=complex<double>(nt,0.0);
            in[1]=complex<double>(nt,0.0);
            in[2]=complex<double>(0.0,0.0);
            
            in[3]=complex<double>(0.0,0.0);
            in[4]=complex<double>(0.0,0.0);
            in[5]=complex<double>(0.0,0.0);
            
            in[6]=complex<double>(0.0,0.0);
            in[7]=complex<double>(nt,0.0);
            in[8]=complex<double>(nt,0.0);
            in[9]=complex<double>(0.0,0.0);
            
            in[10]=complex<double>(0.0,0.0);
            in[11]=complex<double>(0.0,0.0);
            in[12]=complex<double>(0.0,0.0);
            in[13]=complex<double>(0.0,0.0);
            in[14]=complex<double>(0.0,0.0);
            in[15]=complex<double>(0.0,0.0);
            }
        
        void Derivative(const complex<double>* in,complex<double>* out,const double t)
            {
            eps=epsm;
            out[0]=-gam*in[0]+2.0*eps*in[2].__im+gam*nt;
            out[1]=-gam*in[1]+2.0*eps*in[2].__im+gam*nt;
            out[2]=complex<double>(-gam-D,-2.0*teta)*in[2]+complex<double>(0.0,eps)*(in[0]+in[1]+1.0);
            
            out[3]=-gam*in[3]+complex<double>(0.0,eps)*(conj(in[4])-in[5]);
            out[4]=complex<double>(-(gam+D),-2*teta)*in[4]+complex<double>(0.0,2.0*eps)*conj(in[3]);
            out[5]=complex<double>(-(gam+D),-2*teta)*in[5]+complex<double>(0.0,2.0*eps)*in[3];
            
            out[6]=complex<double>(-gam,-2.0*teta)*in[6]+complex<double>(0.0,eps)*(in[7]+in[8]+exp(-D*t));
            out[7]=(-gam-D)*in[7]+complex<double>(0.0,eps)*(in[9]-in[6])+gam*nt*exp(-D*t);
            out[8]=(-gam-D)*in[8]+complex<double>(0.0,eps)*(in[9]-in[6])+gam*nt*exp(-D*t);
            out[9]=complex<double>(-gam-4.0*D,2.0*teta)*in[9]-complex<double>(0.0,eps)*(in[7]+in[8]+exp(-D*t));
            
            out[10]=complex<double>(-gam,-2*teta)*in[10]+complex<double>(0.0,2.0*eps)*conj(in[13]);
            out[11]=complex<double>(-gam,-2*teta)*in[11]+complex<double>(0.0,2.0*eps)*in[12];
            out[12]=-(gam+D)*in[12]-complex<double>(0.0,eps)*(in[11]-conj(in[14]));
            out[13]=-(gam+D)*in[13]-complex<double>(0.0,eps)*(in[15]-conj(in[10]));
            out[14]=complex<double>(-gam-4.0*D,-2*teta)*in[14]+complex<double>(0.0,2.0*eps)*conj(in[12]);
            out[15]=complex<double>(-gam-4.0*D,-2*teta)*in[15]+complex<double>(0.0,2.0*eps)*in[13];
            }
    
        bool Output(const complex<double>* in,const double t)
            {
            if(in[0].__re+in[1].__re>1e10)
                {
                //std::cout<<OutputNum<<std::endl;
                return false;
                }
            output[0][OutputNum]=Q*gam*t;///0.0005;
            output[1][OutputNum]=in[0].__re;
            output[2][OutputNum]=in[1].__re;
            output[3][OutputNum]=in[3].__re;
            output[4][OutputNum]=in[3].__im;
            output[5][OutputNum]=2*sqrt(in[3].__im*in[3].__im+in[3].__re*in[3].__re)/(in[0].__re+in[1].__re);
            output[6][OutputNum]=in[6].__re;
            output[7][OutputNum]=in[6].__im;
            output[8][OutputNum]=2*sqrt(in[6].__im*in[6].__im+in[6].__re*in[6].__re)/(in[0].__re+in[1].__re);
            
            Sigm=__float128(in[0].__re+0.5)*__float128(in[0].__re+0.5)+__float128(in[1].__re+0.5)*__float128(in[1].__re+0.5)+
                    2.0*(__float128(in[6].__im)*__float128(in[6].__im)+__float128(in[6].__re)*__float128(in[6].__re))-
                    2.0*(__float128(in[3].__im)*__float128(in[3].__im)+__float128(in[3].__re)*__float128(in[3].__re))-
                    (__float128(in[10].__im)*__float128(in[10].__im)+__float128(in[10].__re)*__float128(in[10].__re))-
                    (__float128(in[11].__im)*__float128(in[11].__im)+__float128(in[11].__re)*__float128(in[11].__re));
                    
            Mat16[0]=in[0].__re+0.5-in[10].__re;
            Mat16[1]=-in[10].__im;
            Mat16[2]=-in[6].__re+in[3].__re;
            Mat16[3]=-in[6].__im+in[3].__im;
            Mat16[4]=-in[10].__im;
            Mat16[5]=in[0].__re+0.5+in[10].__re;
            Mat16[6]=-in[6].__im-in[3].__im;
            Mat16[7]=in[6].__re+in[3].__re;
            Mat16[8]=-in[6].__re+in[3].__re;
            Mat16[9]=-in[6].__im-in[3].__im;
            Mat16[10]=in[1].__re+0.5-in[11].__re;
            Mat16[11]=-in[11].__im;
            Mat16[12]=-in[6].__im+in[3].__im;
            Mat16[13]=in[6].__re+in[3].__re;
            Mat16[14]=-in[11].__im;
            Mat16[15]=in[1].__re+0.5+in[11].__re;
            
            
                
            Det=Det16();
            output[9][OutputNum]=-log(2*sqrtq(0.5*(Sigm-sqrtq(Sigm*Sigm-4*Det))))/log(2.0);
            if(output[9][OutputNum]<0)
                output[9][OutputNum]=0.0;
            OutputNum++;
            return true;
            }
    
    public:
        MyCalculator():params_pointer(this),outputs(this)
            {
            Mat16.resize(16);
            Mat9.resize(9);
            Mat4.resize(4);
            
            OutputNum=0;
            output.resize(10);
            runge_params.Dimension=16;
            runge_params.Init=std::bind(&MyCalculator::Init,this,std::placeholders::_1);
            runge_params.Derivative=std::bind(&MyCalculator::Derivative,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3);
            runge_params.OutputMaker=std::bind(&MyCalculator::Output,this,std::placeholders::_1,std::placeholders::_2);
            }
        
        virtual ~MyCalculator(){};
        
        virtual BaseContainer<IParameter> & params(void)
            {
            return params_pointer;
            }
        
        virtual BaseContainer<IOutputData> & calculate(void)
            {
            OutputNum=0;
            for(auto it=output.begin();it!=output.end();++it)
                it->resize(int(Nmax/dN));
            
            runge_params.NumTimeSteps=int(Nmax);
            runge_params.TimeStep=dt;
            runge_params.OutputNumTimeSteps=int(dN);
            runge.Init(runge_params);
            D=NW*gam*Q;
            runge.Calculate();
            return outputs;
            }
    };
)


class MyCalculatorFabric:public ICalculatorFabric
    {
    private:
        class ParameterDescrCont:public BaseContainer<IParameterDescr>
            {
            public:
                ParameterDescrCont()
                    {
                    AddItem("gam",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("gam",
                            "interaction power whith the bath")));
                    AddItem("eps",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("eps",
                            "parametric pumping power")));
                    AddItem("nt",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("nt",
                            "Number of equlibrium photons")));
                    AddItem("g",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("g",
                            "Linear interaction coefficient ")));
                    AddItem("D",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("D",
                            "Spectral noise width of the pumping")));
                    AddItem("teta",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("teta",
                            "Frequency detuning nu-om")));
                    AddItem("dt",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("dt",
                            "Time step of Runge-Kutt method")));
                    AddItem("Nmax",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("Nmax",
                            "Number of steps of Runge-Kutt solver")));
                    AddItem("dN",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("dN",
                            "Number of steps through which output is calculating")));
                    }
                ~ParameterDescrCont(){};
            };
    
        class OutputDescrCont:public BaseContainer<IOutputDescr>
            {
            public:    
                OutputDescrCont()
                    {
                    AddItem("t",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("t",
                            "Time")));
                    AddItem("I1",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("I1",
                            "Number of quants in the first oscillator")));
                    AddItem("I2",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("I2",
                            "Number of quants in the second oscillator")));
                    AddItem("ak1*a2_re",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("ak1*a2_re",
                            "Real part of the correlator ak1*a2")));
                    AddItem("ak1*a2_im",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("ak1*a2_im",
                            "Imaginary part of the correlator ak1*a2")));
                    AddItem("alph",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("alph",
                            "Entanglement measure")));
                    AddItem("a1*a2_re",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("a1*a2_re",
                            "Real part of the correlator a1*a2")));
                    AddItem("a1*a2_im",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("a1*a2_im",
                            "Imaginary part of the correlator a1*a2")));
                    AddItem("alph1",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("alph1",
                            "One more entanglement measure")));
                    AddItem("En",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("En",
                            "logarithmic negativity")));
                    }
                ~OutputDescrCont(){};
            };
        
        
        ParameterDescrCont parameter_descr;
        OutputDescrCont output_descr;
        task_description descr;
    
    public:
        MyCalculatorFabric():descr("label")
            {
            }
        ~MyCalculatorFabric(){};
        virtual const std::string& get_task_description(void)
            {
            return descr.to_string();
            }
        virtual BaseContainer<IParameterDescr> & get_parametr_descr(void)
            {
            return parameter_descr;
            }
        virtual BaseContainer<IOutputDescr> & get_output_descr(void)
            {
            return output_descr;
            }
        virtual std::shared_ptr<ICalculator> get_calculator_instance(void)
            {
            return std::shared_ptr<ICalculator>(new MyCalculator);
            }
    };


class MyCalculator1:public ICalculator
    {
    private:
        class ParametrContainer:public BaseContainer<IParameter>
            {
            public:
                ParametrContainer(MyCalculator1 *in)
                    {
                    AddItem("gam",std::shared_ptr<IParameter>(new ParameterBase("gam",&in->gam)));
                    AddItem("Q",std::shared_ptr<IParameter>(new ParameterBase("Q",&in->Q)));
                    AddItem("NoiseWidth",std::shared_ptr<IParameter>(new ParameterBase("NoiseWidth",&in->NW)));
                    AddItem("teta",std::shared_ptr<IParameter>(new ParameterBase("teta",&in->teta)));
                    AddItem("dt",std::shared_ptr<IParameter>(new ParameterBase("dt",&in->dt)));
                    AddItem("Nmax",std::shared_ptr<IParameter>(new ParameterBase("Nmax",&in->Nmax)));
                    AddItem("dN",std::shared_ptr<IParameter>(new ParameterBase("dN",&in->dN)));
                    }
            
                ~ParametrContainer(){};
            };
        
        class OutputOne:public IOutputData
            {
            private:
                MyCalculator1 * parent;
                unsigned int out_id;
                std::string name;
            public:
                OutputOne(const std::string &na,MyCalculator1 *p,unsigned int id):
                                        parent(p),out_id(id),name(na)
                    {
                    }
                virtual ~OutputOne(){};
                virtual std::string GetName(void)
                    {
                    return name;
                    }
                
                virtual unsigned int GetCount(void)
                    {
                    return parent->output[out_id].size();
                    }
                virtual std::list<unsigned int> GetDimensions(void)
                    {
                    std::list<unsigned int> tmp;
                    if(out_id==0 || out_id==1)
                        {
                        tmp.push_back(parent->output[out_id].size());
                        }
                    else
                        {
                        tmp.push_back(parent->Neps);
                        tmp.push_back(parent->Nnt);
                        }
                    return tmp;
                    }
                
                virtual std::vector<double>& GetData(void)
                    {
                    return parent->output[out_id];
                    }
            };
        
        class OutputContainer:public BaseContainer<IOutputData>
            {
            public:    
                OutputContainer(MyCalculator1 *in)
                    {
                    AddItem("eps",std::shared_ptr<IOutputData>
                    (new OutputOne("eps",in,0)));
                    AddItem("nt",std::shared_ptr<IOutputData>
                    (new OutputOne("nt",in,1)));
                    AddItem("entangled time",std::shared_ptr<IOutputData>
                    (new OutputOne("entangled time",in,2)));
                    AddItem("En_max",std::shared_ptr<IOutputData>
                    (new OutputOne("En_max",in,3)));
                    AddItem("ETC",std::shared_ptr<IOutputData>
                    (new OutputOne("ETC",in,4)));
                    }
                ~OutputContainer(){};
            };
        
        
        
        
        unsigned int OutputNum;
        
        
        double gam;
        
        
        double Q;
        double NW;
        double teta;
        double dt;
        double Nmax;
        double dN;
        
        
        ParametrContainer params_pointer;
        OutputContainer outputs;
    
        std::vector<std::vector<double> > output;
    
        unsigned int Neps;
        unsigned int Nnt;
        unsigned int NTh;
        
        double eps_first;
        double eps_last;
        double eps_delt;
        double nt_first;
        double nt_last;
        double nt_delt;
        
        std::thread *workers;
        
        struct  results
            {
            unsigned int t_begin;
            unsigned int t_end;
            double En_max;
            };
        
        results data_proc(const std::vector<double>& in,unsigned int count)
            {
            results tmp;
            unsigned int state=0;
            tmp.En_max=0.0;
            tmp.t_begin=0;
            tmp.t_end=0;
            for(unsigned int peri=0;peri<count;++peri)
                {
                if(in[peri]>tmp.En_max)
                    tmp.En_max=in[peri];
                switch(state)
                    {
                    case 0:
                        if(in[peri]>1e-6)
                            {
                            tmp.t_begin=peri;
                            tmp.t_end=peri;
                            state=1;
                            }
                        break;
                    case 1:
                        tmp.t_end=peri;
                        if(in[peri]<1e-4)
                            {
                            state=2;
                            }
                        break;
                    case 2:
                        break;
                    }
                }
            return tmp;
            }
        
        void worker_func(unsigned int Nbegin,unsigned int col)
            {
            MyCalculator calc;
            calc.params()["gam"].SetValue(gam);
            calc.params()["Q"].SetValue(Q);
            calc.params()["NoiseWidth"].SetValue(NW);
            calc.params()["teta"].SetValue(teta);
            calc.params()["dt"].SetValue(dt);
            calc.params()["Nmax"].SetValue(Nmax);
            calc.params()["dN"].SetValue(dN);
            double nt;
            double eps=eps_first;
            results results_tmp;
            for(unsigned int peri1=0;peri1<Neps;++peri1)
                {
                nt=nt_first+nt_delt*double(Nbegin);
                for(unsigned int peri2=Nbegin;peri2<(Nbegin+col);++peri2)
                    {
                    calc.params()["eps"].SetValue(eps);
                    calc.params()["nt"].SetValue(nt);
                    BaseContainer<IOutputData>& output_data=calc.calculate();
                    results_tmp=data_proc(output_data["En"].GetData(),output_data["En"].GetCount());
                    output[3][Neps*peri2+peri1]=results_tmp.En_max;
                    output[2][Neps*peri2+peri1]=output_data["t"].GetData()[results_tmp.t_end]-output_data["t"].GetData()[results_tmp.t_begin];
                    if(results_tmp.t_end>=output_data["t"].GetCount()-1)
                        output[4][Neps*peri2+peri1]=0.0;
                    else
                        output[4][Neps*peri2+peri1]=1.0;
                    nt+=nt_delt;
                    }
                eps+=eps_delt;
                }
            }
    
    public:
        MyCalculator1():params_pointer(this),outputs(this)
            {
            OutputNum=0;
            output.resize(5);
            Neps=100;
            Nnt=100;
            NTh=20;
            eps_first=0.0;
            eps_last=100.0;
            nt_first=0.0;
            nt_last=50.0;
            eps_delt=(eps_last-eps_first)/double(Neps);
            nt_delt=(nt_last-nt_first)/double(Nnt);
            }
        
        virtual ~MyCalculator1()
            {
            }
        
        virtual BaseContainer<IParameter> & params(void)
            {
            return params_pointer;
            }
        
        virtual BaseContainer<IOutputData> & calculate(void)
            {
            OutputNum=0;
            output[0].resize(Neps);
            output[1].resize(Nnt);
            output[2].resize(Neps*Nnt);
            output[3].resize(Neps*Nnt);
            output[4].resize(Neps*Nnt);
            output[0][0]=eps_first;
            for(unsigned int peri=1;peri<Neps;++peri)
                {
                output[0][peri]=output[0][peri-1]+eps_delt;
                }
            output[1][0]=nt_first;
            for(unsigned int peri=1;peri<Nnt;++peri)
                {
                output[1][peri]=output[1][peri-1]+nt_delt;
                }
            workers=new std::thread[NTh];
            unsigned int Nnt_current=0;
            for(unsigned int peri=0;peri<NTh;++peri)
                {
                if(peri<Nnt%NTh)
                    {
                    workers[peri]=std::thread(&MyCalculator1::worker_func,this,Nnt_current,Nnt/NTh+1);
                    Nnt_current+=Nnt/NTh+1;
                    }
                else
                    {
                    workers[peri]=std::thread(&MyCalculator1::worker_func,this,Nnt_current,Nnt/NTh);
                    Nnt_current+=Nnt/NTh;
                    }
                }
            
            for(unsigned int peri=0;peri<NTh;++peri)
                {
                workers[peri].join();
                }
            delete[] workers;
            return outputs;
            }
    };





int main(int argc,char ** argv)
    {
        
    
    MyCalculator1 calc;
    archive::archiver_v1 arc("res/data1.bin","Map of entangeld time through nt and eps",task_description("label").to_string());
    std::shared_ptr<archive::IDataWriter> dth=arc.GetWriterInstance();
    calc.params()["gam"].SetValue(1.0);
    calc.params()["Q"].SetValue(5000);
    calc.params()["NoiseWidth"].SetValue(1e-11);
    calc.params()["teta"].SetValue(000.00);
    calc.params()["dt"].SetValue(0.00001);
    calc.params()["Nmax"].SetValue(10000000.0);
    calc.params()["dN"].SetValue(10.0);
    dth->AddRecord("",calc.params(),calc.calculate());
    //Gnuplot gp;
	//// Create a script which can be manually fed into gnuplot later:
	////    Gnuplot gp(">script.gp");
	//// Create script and also feed to gnuplot:
	////    Gnuplot gp("tee plot.gp | gnuplot -persist");
	//// Or choose any of those options at runtime by setting the GNUPLOT_IOSTREAM_CMD
	//// environment variable.

	//// Gnuplot vectors (i.e. arrows) require four columns: (x,y,dx,dy)
	

	//// You could also use:
	////   std::vector<std::vector<double> >
	////   boost::tuple of four std::vector's
	////   std::vector of std::tuple (if you have C++11)
	////   arma::mat (with the Armadillo library)
	////   blitz::Array<blitz::TinyVector<double, 4>, 1> (with the Blitz++ library)
	//// ... or anything of that sort

	
    //// Don't forget to put "\n" at the end of each line!
	
    //gp << "set logscale x\n";
    //gp << "set xrange [1:"<<(*out_tmp)["I1"].GetData()[(*out_tmp)["t"].GetCount()-1]/1.0<<"]\nset yrange [0:10]\n";
	//// '-' means read from stdin.  The send1d() function sends data to gnuplot's stdin.
	//gp << "plot '-' with lines title 'pts_B'\n";
	////gp.send1d(boost::make_tuple((*out_tmp)["I2"].GetData(), (*out_tmp)["En"].GetData(), (*out_tmp)["I1"].GetData(), (*out_tmp)["I1"].GetData()));
    //gp.send1d(boost::make_tuple((*out_tmp)["I1"].GetData(), (*out_tmp)["En"].GetData(), (*out_tmp)["t"].GetData(), (*out_tmp)["t"].GetData()));
    ////std::cout<<(*out_tmp)["t"].GetData().size()<<std::endl;
    
    ///*for(unsigned int k=0;k<(*out_tmp)["t"].GetCount();k++)
        //{
        //std::cout<<(*out_tmp)["En"].GetData()[k] <<" ";
        //}
    //*/
    
    ////std::cout<<fabr.get_task_description()<<std::endl;
    
    return 0;
    }
