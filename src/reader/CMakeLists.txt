cmake_minimum_required(VERSION 3.0)

project(reader)
set (PROJECT reader)
set (SOURCES main.cpp)
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -std=c++11 -O0 -pg")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=c++11 ")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY "../../bin/reader/")
find_package (Boost COMPONENTS system filesystem iostreams thread program_options REQUIRED)
find_package (Threads)

include_directories (../CompManag/include)

add_executable(${PROJECT} ${SOURCES})

add_subdirectory(../CompManag/code/libs CompManag/lib)


target_link_libraries (${PROJECT} ${CMAKE_THREAD_LIBS_INIT} 
                                  CompManag
                                  quadmath
                                  ${Boost_LIBRARIES})
