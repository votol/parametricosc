#include <iostream>
#include <boost/tuple/tuple.hpp>
#include "complex.h"
#include "CompManag.hpp"
#include "gnuplot-iostream.h"
#include <fstream>


using namespace CompManag;

class reader
    {
    public:
        class data_record
            {
                class data_record_params:public BaseContainer<IParameter>
                    {
                        std::vector<double> params_value;
                    public:
                        data_record_params(std::ifstream& in)
                            {
                            if(!in.eof())
                                {
                                unsigned int params_num;
                                unsigned int str_length;
                                char* str_tmp;
                                double param_value;
                                in.read((char*)&params_num,4);
                                params_value.resize(params_num);
                                for(unsigned int peri=0;peri<params_num;++peri)
                                    {
                                    in.read((char*)&str_length,4);
                                    str_tmp=new char[str_length];
                                    in.read(str_tmp,str_length);
                                    in.read((char*)&param_value,8);
                                    params_value[peri]=param_value;
                                    AddParameter(std::string(str_tmp,str_length),std::shared_ptr<IParameter>(
                                                new ParameterBase(std::string(str_tmp,str_length),&(params_value[peri]))));
                                    delete[] str_tmp;
                                    }
                                }
                            }
                        ~data_record_params()
                            {
                            }
                    };
                
                class data_record_output:public BaseContainer<IOutputData>
                    {
                        class one_data:public IOutputData
                            {
                                std::string name;
                                std::vector<double> data;
                            public:
                                one_data(std::ifstream& in)
                                    {
                                    unsigned int str_length;
                                    char* str_tmp;
                                    in.read((char*)&str_length,4);
                                    str_tmp=new char[str_length];
                                    in.read(str_tmp,str_length);
                                    name=std::string(str_tmp,str_length);
                                    unsigned int num_elements;
                                    double tmp_element;
                                    in.read((char*)&num_elements,4);
                                    data.resize(num_elements);
                                    for(unsigned int peri=0;peri<num_elements;++peri)
                                        {
                                        in.read((char*)&tmp_element,8);
                                        data[peri]=tmp_element;
                                        }
                                    }
                                virtual ~one_data()
                                    {
                                    
                                    }
                                virtual std::string GetName(void)
                                    {
                                    return name;
                                    }
                                virtual unsigned int GetCount(void)
                                    {
                                    return data.size();
                                    }
                                virtual std::vector<double>& GetData(void)
                                    {
                                    return data;
                                    }
                            };
                    
                    
                    public:
                        data_record_output(std::ifstream& in)
                            {
                            if(!in.eof())
                                {
                                unsigned int outputs_num;
                                in.read((char*)&outputs_num,4);
                                for(unsigned int peri=0;peri<outputs_num;++peri)
                                    {
                                    std::shared_ptr<IOutputData> output(new one_data(in));
                                    AddParameter(output->GetName(),output);
                                    }
                                }
                            }
                        ~data_record_output()
                            {
                            }
                    };
                
                data_record_params params_c;
                data_record_output data_c;
            public:
                data_record(std::ifstream& in):params_c(in),data_c(in)
                    {
                    }
                
                ~data_record()
                    {
                    }
                BaseContainer<IParameter>& params(void)
                    {
                    return params_c;
                    }
            
                BaseContainer<IOutputData>& data(void)
                    {
                    return data_c;
                    }
            };
    private:
        std::ifstream i_st;
        data_record* current_record;
    public:
        
        reader(const std::string file_name)
            {
            i_st.open(file_name,std::ifstream::in | std::ifstream::binary);
            current_record=new data_record(i_st);
            }
        ~reader()
            {
            delete current_record;
            i_st.close();
            }
        reader& operator++()
            {
            delete current_record;
            current_record=new data_record(i_st);
            return *this;
            }
        data_record* operator->() const
            {
            return current_record;
            }
    };

class matrix
    {
        std::vector<std::vector<double> > data;
    public:
        matrix(unsigned int col,unsigned int row,const std::vector<double>& vec)
            {
            data.resize(row);
            for(unsigned int peri=0;peri<row;peri++)
                data[peri].resize(col);
            for(unsigned int peri1=0;peri1<row;peri1++)
                for(unsigned int peri2=0;peri2<col;peri2++)
                    data[peri1][peri2]=vec[peri1*col+peri2]/(2.0*M_PI);
            }
        ~matrix()
            {
            }
        std::vector<double>& operator[] (unsigned int j)
            {
            return data[j];
            }
    };


int main(int argc,char ** argv)
    {
    reader probe("res/data1.bin");
    for(auto it=probe->params().begin();it!=probe->params().end();++it)
        {
        std::cout<<it->GetName()<<" "<<it->GetValue()<<std::endl;
        }
    std::cout<<"******************************************\n";
    for(auto it=probe->data().begin();it!=probe->data().end();++it)
        {
        std::cout<<it->GetName()<<" "<<it->GetCount()<<std::endl;
        }
    std::cout<<"******************************************\n";
    
    unsigned int count1=0;
    for(unsigned int peri=0;peri<probe->data()["ETC"].GetCount();++peri)
        {
        if(probe->data()["ETC"].GetData()[peri]==0.0)
            {
            std::cout<<"eps="<<probe->data()["eps"].GetData()[peri%100]<<" ";
            std::cout<<"nt="<<probe->data()["nt"].GetData()[peri/100]<<std::endl;
            count1++;
            }
        }
    std::cout<<count1<<std::endl;
    std::cout<<"******************************************\n";
    
    matrix mt(100,100,probe->data()["entangled time"].GetData());
    
    Gnuplot gp;
	// Create a script which can be manually fed into gnuplot later:
	//    Gnuplot gp(">script.gp");
	// Create script and also feed to gnuplot:
	//    Gnuplot gp("tee plot.gp | gnuplot -persist");
	// Or choose any of those options at runtime by setting the GNUPLOT_IOSTREAM_CMD
	// environment variable.

	// Gnuplot vectors (i.e. arrows) require four columns: (x,y,dx,dy)
	

	// You could also use:
	//   std::vector<std::vector<double> >
	//   boost::tuple of four std::vector's
	//   std::vector of std::tuple (if you have C++11)
	//   arma::mat (with the Armadillo library)
	//   blitz::Array<blitz::TinyVector<double, 4>, 1> (with the Blitz++ library)
	// ... or anything of that sort

	
    // Don't forget to put "\n" at the end of each line!
	
    //gp << "set logscale x\n";
    gp << "set xrange [0:"<<probe->data()["eps"].GetData()[probe->data()["eps"].GetCount()-1]<<"]\nset yrange [0:1000]\n";
	// '-' means read from stdin.  The send1d() function sends data to gnuplot's stdin.
	gp << "plot '-' with lines, '-' with lines, '-' with lines title 'pts_B'\n";
	//gp.send1d(boost::make_tuple((*out_tmp)["I2"].GetData(), (*out_tmp)["En"].GetData(), (*out_tmp)["I1"].GetData(), (*out_tmp)["I1"].GetData()));
    gp.send1d(boost::make_tuple(probe->data()["eps"].GetData(), mt[10], probe->data()["eps"].GetData(), probe->data()["eps"].GetData()));
    //std::cout<<(*out_tmp)["t"].GetData().size()<<std::endl;
        
    gp.send1d(boost::make_tuple(probe->data()["eps"].GetData(), mt[30], probe->data()["eps"].GetData(), probe->data()["eps"].GetData()));
    /*for(unsigned int k=0;k<(*out_tmp)["t"].GetCount();k++)
        {
        std::cout<<(*out_tmp)["En"].GetData()[k] <<" ";
        }
    */
    
    //std::cout<<fabr.get_task_description()<<std::endl;
    
    
    
    return 0;
    }
