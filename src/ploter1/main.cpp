#include <iostream>
#include <boost/tuple/tuple.hpp>
extern "C" {
#include <quadmath.h>
}
#include "complex.h"
#include "CompManag.hpp"
#include "gnuplot-iostream.h"
#include <fstream>


using namespace CompManag;

make_description(label,
class MyCalculator:public ICalculator
    {
    private:
        class ParametrContainer:public BaseContainer<IParameter>
            {
            public:
                ParametrContainer(MyCalculator *in)
                    {
                    AddItem("gam",std::shared_ptr<IParameter>(new ParameterBase("gam",&in->gam)));
                    AddItem("eps",std::shared_ptr<IParameter>(new ParameterBase("eps",&in->epsm)));
                    AddItem("nt",std::shared_ptr<IParameter>(new ParameterBase("nt",&in->nt)));
                    AddItem("Q",std::shared_ptr<IParameter>(new ParameterBase("Q",&in->Q)));
                    AddItem("NoiseWidth",std::shared_ptr<IParameter>(new ParameterBase("NoiseWidth",&in->NW)));
                    AddItem("teta",std::shared_ptr<IParameter>(new ParameterBase("teta",&in->teta)));
                    AddItem("dt",std::shared_ptr<IParameter>(new ParameterBase("dt",&in->dt)));
                    AddItem("Nmax",std::shared_ptr<IParameter>(new ParameterBase("Nmax",&in->Nmax)));
                    AddItem("dN",std::shared_ptr<IParameter>(new ParameterBase("dN",&in->dN)));
                    }
            
                ~ParametrContainer(){};
            };
        
        class OutputOne:public IOutputData
            {
            private:
                MyCalculator * parent;
                unsigned int out_id;
                std::string name;
            public:
                OutputOne(const std::string &na,MyCalculator *p,unsigned int id):
                                        parent(p),out_id(id),name(na)
                    {
                    }
                virtual ~OutputOne(){};
                virtual std::string GetName(void)
                    {
                    return name;
                    }
                
                virtual unsigned int GetCount(void)
                    {
                    return parent->OutputNum;
                    }
                
                virtual std::list<unsigned int> GetDimensions(void)
					{
					std::list<unsigned int> tmp;
					tmp.push_back(parent->OutputNum);
					return tmp;
					}
                virtual std::vector<double>& GetData(void)
                    {
                    return parent->output[out_id];
                    }
            };
        
        class OutputContainer:public BaseContainer<IOutputData>
            {
            public:    
                OutputContainer(MyCalculator *in)
                    {
                    AddItem("t",std::shared_ptr<IOutputData>
                    (new OutputOne("t",in,0)));
                    AddItem("I1",std::shared_ptr<IOutputData>
                    (new OutputOne("I1",in,1)));
                    AddItem("I2",std::shared_ptr<IOutputData>
                    (new OutputOne("I2",in,2)));
                    AddItem("ak1*a2_re",std::shared_ptr<IOutputData>
                    (new OutputOne("ak1*a2_re",in,3)));
                    AddItem("ak1*a2_im",std::shared_ptr<IOutputData>
                    (new OutputOne("ak1*a2_im",in,4)));
                    AddItem("alph",std::shared_ptr<IOutputData>
                    (new OutputOne("alph",in,5)));
                    AddItem("a1*a2_re",std::shared_ptr<IOutputData>
                    (new OutputOne("a1*a2_re",in,6)));
                    AddItem("a1*a2_im",std::shared_ptr<IOutputData>
                    (new OutputOne("a1*a2_im",in,7)));
                    AddItem("alph1",std::shared_ptr<IOutputData>
                    (new OutputOne("alph1",in,8)));
                    AddItem("En",std::shared_ptr<IOutputData>
                    (new OutputOne("En",in,9)));
                    }
                ~OutputContainer(){};
            };
        
        
        
        
        unsigned int OutputNum;
        Runge::RungeSolver<complex<double> > runge;
        Runge::RungeSolverInit<complex<double> > runge_params;
        
        
        double gam;
        
        double eps;
        double epsm;
        
        double Q;
        double nt;
        double D;
        double NW;
        double teta;
        double dt;
        double Nmax;
        double dN;
        
        
        //tmp
        __float128 Sigm;
        std::vector<__float128> Mat16;
        std::vector<__float128> Mat9;
        std::vector<__float128> Mat4;
        __float128 Det;
        
        ParametrContainer params_pointer;
        OutputContainer outputs;
    
        std::vector<std::vector<double> > output;
    
        __float128 Det4(void)
            {
            return Mat4[0]*Mat4[3]-Mat4[1]*Mat4[2];
            }
        
        
        __float128 Det9(void)
            {
            __float128 sum=0;
            __float128 koe=1;
            for(unsigned int i=0;i<3;i++)
                {
                unsigned int l=0;
                for(unsigned int j=0;j<3;j++)
                    for(unsigned int k=1;k<3;k++)
                        if(j!=i)
                            {
                            Mat4[l]=Mat9[3*k+j];
                            l++;
                            }
                sum+=Mat9[i]*koe*Det4();
                koe*=-1;
                }
            return sum;
            }
        
        __float128 Det16(void)
            {
            __float128 sum=0;
            __float128 koe=1;
            for(unsigned int i=0;i<4;i++)
                {
                unsigned int l=0;
                for(unsigned int j=0;j<4;j++)
                    for(unsigned int k=1;k<4;k++)
                        if(j!=i)
                            {
                            Mat9[l]=Mat16[4*k+j];
                            l++;
                            }
                sum+=Mat16[i]*koe*Det9();
                koe*=-1;
                }
            return sum;
            }
        
        
        void Init(complex<double> * in)
            {
            in[0]=complex<double>(nt,0.0);
            in[1]=complex<double>(nt,0.0);
            in[2]=complex<double>(0.0,0.0);
            
            in[3]=complex<double>(0.0,0.0);
            in[4]=complex<double>(0.0,0.0);
            in[5]=complex<double>(0.0,0.0);
            
            in[6]=complex<double>(0.0,0.0);
            in[7]=complex<double>(nt,0.0);
            in[8]=complex<double>(nt,0.0);
            in[9]=complex<double>(0.0,0.0);
            
            in[10]=complex<double>(0.0,0.0);
            in[11]=complex<double>(0.0,0.0);
            in[12]=complex<double>(0.0,0.0);
            in[13]=complex<double>(0.0,0.0);
            in[14]=complex<double>(0.0,0.0);
            in[15]=complex<double>(0.0,0.0);
            }
        
        void Derivative(const complex<double>* in,complex<double>* out,const double t)
            {
            eps=epsm;
            out[0]=-gam*in[0]+2.0*eps*in[2].__im+gam*nt;
            out[1]=-gam*in[1]+2.0*eps*in[2].__im+gam*nt;
            out[2]=complex<double>(-gam-D,-2.0*teta)*in[2]+complex<double>(0.0,eps)*(in[0]+in[1]+1.0);
            
            out[3]=-gam*in[3]+complex<double>(0.0,eps)*(conj(in[4])-in[5]);
            out[4]=complex<double>(-(gam+D),-2*teta)*in[4]+complex<double>(0.0,2.0*eps)*conj(in[3]);
            out[5]=complex<double>(-(gam+D),-2*teta)*in[5]+complex<double>(0.0,2.0*eps)*in[3];
            
            out[6]=complex<double>(-gam,-2.0*teta)*in[6]+complex<double>(0.0,eps)*(in[7]+in[8]+exp(-D*t));
            out[7]=(-gam-D)*in[7]+complex<double>(0.0,eps)*(in[9]-in[6])+gam*nt*exp(-D*t);
            out[8]=(-gam-D)*in[8]+complex<double>(0.0,eps)*(in[9]-in[6])+gam*nt*exp(-D*t);
            out[9]=complex<double>(-gam-4.0*D,2.0*teta)*in[9]-complex<double>(0.0,eps)*(in[7]+in[8]+exp(-D*t));
            
            out[10]=complex<double>(-gam,-2*teta)*in[10]+complex<double>(0.0,2.0*eps)*conj(in[13]);
            out[11]=complex<double>(-gam,-2*teta)*in[11]+complex<double>(0.0,2.0*eps)*in[12];
            out[12]=-(gam+D)*in[12]-complex<double>(0.0,eps)*(in[11]-conj(in[14]));
            out[13]=-(gam+D)*in[13]-complex<double>(0.0,eps)*(in[15]-conj(in[10]));
            out[14]=complex<double>(-gam-4.0*D,-2*teta)*in[14]+complex<double>(0.0,2.0*eps)*conj(in[12]);
            out[15]=complex<double>(-gam-4.0*D,-2*teta)*in[15]+complex<double>(0.0,2.0*eps)*in[13];
            }
    
        bool Output(const complex<double>* in,const double t)
            {
            if(in[0].__re+in[1].__re>1e10)
                {
                //std::cout<<OutputNum<<std::endl;
                return false;
                }
            output[0][OutputNum]=Q*gam*t;///0.0005;
            output[1][OutputNum]=in[0].__re;
            output[2][OutputNum]=in[1].__re;
            output[3][OutputNum]=in[3].__re;
            output[4][OutputNum]=in[3].__im;
            output[5][OutputNum]=2*sqrt(in[3].__im*in[3].__im+in[3].__re*in[3].__re)/(in[0].__re+in[1].__re);
            output[6][OutputNum]=in[6].__re;
            output[7][OutputNum]=in[6].__im;
            output[8][OutputNum]=2*sqrt(in[6].__im*in[6].__im+in[6].__re*in[6].__re)/(in[0].__re+in[1].__re);
            
            Sigm=__float128(in[0].__re+0.5)*__float128(in[0].__re+0.5)+__float128(in[1].__re+0.5)*__float128(in[1].__re+0.5)+
                    2.0*(__float128(in[6].__im)*__float128(in[6].__im)+__float128(in[6].__re)*__float128(in[6].__re))-
                    2.0*(__float128(in[3].__im)*__float128(in[3].__im)+__float128(in[3].__re)*__float128(in[3].__re))-
                    (__float128(in[10].__im)*__float128(in[10].__im)+__float128(in[10].__re)*__float128(in[10].__re))-
                    (__float128(in[11].__im)*__float128(in[11].__im)+__float128(in[11].__re)*__float128(in[11].__re));
                    
            Mat16[0]=in[0].__re+0.5-in[10].__re;
            Mat16[1]=-in[10].__im;
            Mat16[2]=-in[6].__re+in[3].__re;
            Mat16[3]=-in[6].__im+in[3].__im;
            Mat16[4]=-in[10].__im;
            Mat16[5]=in[0].__re+0.5+in[10].__re;
            Mat16[6]=-in[6].__im-in[3].__im;
            Mat16[7]=in[6].__re+in[3].__re;
            Mat16[8]=-in[6].__re+in[3].__re;
            Mat16[9]=-in[6].__im-in[3].__im;
            Mat16[10]=in[1].__re+0.5-in[11].__re;
            Mat16[11]=-in[11].__im;
            Mat16[12]=-in[6].__im+in[3].__im;
            Mat16[13]=in[6].__re+in[3].__re;
            Mat16[14]=-in[11].__im;
            Mat16[15]=in[1].__re+0.5+in[11].__re;
            
            
                
            Det=Det16();
            output[9][OutputNum]=-log(2*sqrtq(0.5*(Sigm-sqrtq(Sigm*Sigm-4*Det))))/log(2.0);
            if(output[9][OutputNum]<0)
                output[9][OutputNum]=0.0;
            OutputNum++;
            return true;
            }
    
    public:
        MyCalculator():params_pointer(this),outputs(this)
            {
            Mat16.resize(16);
            Mat9.resize(9);
            Mat4.resize(4);
            
            OutputNum=0;
            output.resize(10);
            runge_params.Dimension=16;
            runge_params.Init=std::bind(&MyCalculator::Init,this,std::placeholders::_1);
            runge_params.Derivative=std::bind(&MyCalculator::Derivative,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3);
            runge_params.OutputMaker=std::bind(&MyCalculator::Output,this,std::placeholders::_1,std::placeholders::_2);
            }
        
        virtual ~MyCalculator(){};
        
        virtual BaseContainer<IParameter> & params(void)
            {
            return params_pointer;
            }
        
        virtual BaseContainer<IOutputData> & calculate(void)
            {
            OutputNum=0;
            for(auto it=output.begin();it!=output.end();++it)
                it->resize(int(Nmax/dN));
            
            runge_params.NumTimeSteps=int(Nmax);
            runge_params.TimeStep=dt;
            runge_params.OutputNumTimeSteps=int(dN);
            runge.Init(runge_params);
            D=NW*gam*Q;
            runge.Calculate();
            return outputs;
            }
    };
)

#include <set>
struct record
    {
    std::list<std::string> parameter_names;
    std::list<double> parameter_values;
    std::string data_name;
    std::vector<double> data;
    };

class plot_data_archiver
    {
    public:
        plot_data_archiver(){};
        ~plot_data_archiver(){};
        void serialize(std::ostream& st,const std::list<record>& data)
            {
            int int_tmp;
            for(auto it=data.begin();it!=data.end();++it)
                {
                int_tmp=it->parameter_names.size();
                st.write((char*)&(int_tmp),4);
                auto it2=it->parameter_names.begin();
                for(auto it1=it->parameter_values.begin();it1!=it->parameter_values.end();++it1)
                    {
                    int_tmp=it2->size();
                    st.write((char*)&(int_tmp),4);
                    st.write(it2->data(),int_tmp);
                    st.write((char*)&(*it1),8);
                    ++it2;
                    }
                int_tmp=it->data_name.size();
                st.write((char*)&(int_tmp),4);
                st.write(it->data_name.data(),int_tmp);
                int_tmp=it->data.size();
                st.write((char*)&(int_tmp),4);
                for(auto it1=it->data.begin();it1!=it->data.end();++it1)
                    {
                    st.write((char*)&(*it1),8);
                    }
                }
            }
        void deserialize(std::istream& st,std::list<record>& data)
            {
            int int_tmp,int_tmp1;
            int current_string_length=1; 
            char* tmp_string=new char[1];
            while(!st.eof())
                {
                data.push_back(record());
                st.read((char*)&(int_tmp),4);
                for(int count1=0;count1<int_tmp;++count1)
                    {
                    st.read((char*)&(int_tmp1),4);
                    if(int_tmp1>current_string_length)
                        {
                        current_string_length=int_tmp1;
                        delete [] tmp_string;
                        tmp_string=new char[current_string_length];
                        }
                    st.read(tmp_string,int_tmp1);
                    data.back().parameter_names.push_back(std::string(tmp_string,int_tmp1));
                    data.back().parameter_values.push_back(0.0);
                    st.read((char*)&(data.back().parameter_values.back()),int_tmp1);
                    }
                st.read((char*)&(int_tmp),4);
                if(int_tmp>current_string_length)
                    {
                    current_string_length=int_tmp;
                    delete [] tmp_string;
                    tmp_string=new char[current_string_length];
                    }
                st.read(tmp_string,int_tmp);
                data.back().data_name=std::string(tmp_string,int_tmp);
                st.read((char*)&(int_tmp),4);
                data.back().data.resize(int_tmp);
                st.read((char*)data.back().data.data(),8*int_tmp);
                }
            }
    };

class plot_processor
    {
               
        std::string _output_dir;
        cmd_processor my_cmd;
        plot_data_archiver plot_data_archiver_instance;
        
        MyCalculator a;
        BaseContainer<IOutputData> *out_tmp;
        
        
        unsigned int current_id;
    
        std::set<std::string> file_names;
        
        std::list<std::string> current_block_file_names;
        std::list<record> current_block_records;
    
        void clear(std::list<std::string> in)
            {
            for(auto it=current_block_file_names.begin();it!=current_block_file_names.end();++it)
                {
                file_names.erase(*it);
                }
            current_block_file_names.clear();
            current_block_records.clear();
            }
        
        void save(std::list<std::string> in)
            {
            std::ofstream ofs;
            ofs.open(_output_dir+"data.bin",std::ofstream::out | std::ofstream::app | std::ofstream::binary);
            plot_data_archiver_instance.serialize(ofs,current_block_records);
            ofs.close();
            ofs.open(_output_dir+"data.ini",std::ofstream::out | std::ofstream::app);
            unsigned int id=current_id;
            for(auto it=current_block_file_names.begin();it!=current_block_file_names.end();++it)
                {
                ofs<<"["<<*it<<"]\n";
                ofs<<"x="<<id<<"\n";
                ofs<<"y="<<id+1<<"\n";
                id+=2;
                }
            ofs.close();
            current_id+=current_block_records.size();
            current_block_file_names.clear();
            current_block_records.clear();
            
            }
        
        void plot(std::list<std::string> in)
            {
            out_tmp=&(a.calculate());
            double xmin,xmax,ymin,ymax;
            xmin=(*out_tmp)["t"].GetData()[0];
            xmax=(*out_tmp)["t"].GetData()[(*out_tmp)["t"].GetCount()-1];
            ymin=(*out_tmp)["En"].GetData()[0];
            ymax=(*out_tmp)["En"].GetData()[0];
                {    
                auto it=(*out_tmp)["En"].GetData().begin();
                for(unsigned int peri=0;peri<(*out_tmp)["En"].GetCount();peri++)
                    {
                    if(*it<ymin)
                        ymin=*it;
                    if(*it>ymax)
                        ymax=*it;    
                    ++it;
                    }
                }
                {   
                auto it=current_block_records.begin();
                while(it!=current_block_records.end())
                    {
                    if(xmin>it->data[0])
                        xmin=it->data[0];
                    if(xmax<it->data[it->data.size()-1])
                        xmax=it->data[it->data.size()-1];
                    ++it;
                    for(auto it1=it->data.begin();it1!=it->data.end();++it1)
                        {
                        if(*it1<ymin)
                            ymin=*it1;
                        if(*it1>ymax)
                            ymax=*it1;    
                        }
                    ++it;
                    }
                }
            
            ymax*=1.01;
            Gnuplot gp;
            gp << "set xrange ["<<xmin<<":"<<xmax<<"]\nset yrange ["<<ymin<<":"<<ymax<<"]\n";
            {
                gp<<"plot ";
                auto it=current_block_records.begin();
                while(it!=current_block_records.end())
                    {
                    gp << "'-' lt rgb \"#0000FF\" with lines title 'previewold', ";
                    ++it;
                    ++it;
                    }
                gp<<"'-' lt rgb \"#FF0000\" with lines title 'preview'\n";
            }
            {
                auto it=current_block_records.begin();
                auto it_tmp=it;
                while(it!=current_block_records.end())
                    {
                    it_tmp=it;
                    ++it_tmp;
                    gp.send1d(boost::make_tuple(it->data,it_tmp->data,it->data,it->data));
                    ++it;
                    ++it;
                    }
                gp.send1d(boost::make_tuple((*out_tmp)["t"].GetData(), (*out_tmp)["En"].GetData(), (*out_tmp)["t"].GetData(), (*out_tmp)["t"].GetData()));
            }
            std::string answer;
            while(1)
                {
                std::cout<<"Add line?(y/N):";
                std::cin>>answer;
                if(answer=="" || answer=="n" || answer=="N")
                    {
                    std::cin.get();
                    break;
                    }
                if(answer=="y" || answer=="Y")
                    {
                    while(1)
                        {
                        std::cout<<"Name of the file?:";
                        std::cin>>answer;
                        if(file_names.find(answer)==file_names.end() && answer!="data.bin" && answer != "data.ini")
                            break;
                        std::cout<<"Wrong name\n";
                        }
                    std::cin.get();
                    file_names.insert(answer);
                    current_block_file_names.push_back(answer);
                    current_block_records.push_back(record());
                    for(auto it=a.params().begin();it!=a.params().end();++it)
                        {
                        current_block_records.back().parameter_names.push_back(it->GetName());
                        current_block_records.back().parameter_values.push_back(it->GetValue());
                        }
                    current_block_records.back().data_name="t";
                    current_block_records.back().data.resize((*out_tmp)["t"].GetCount());
                        {
                        auto it1=(*out_tmp)["t"].GetData().begin();
                        for(auto it=current_block_records.back().data.begin();it!=current_block_records.back().data.end();++it)
                            {
                            *it=*it1;
                            ++it1;
                            }
                        }
                    
                    current_block_records.push_back(record());
                    for(auto it=a.params().begin();it!=a.params().end();++it)
                        {
                        current_block_records.back().parameter_names.push_back(it->GetName());
                        current_block_records.back().parameter_values.push_back(it->GetValue());
                        }
                    current_block_records.back().data_name="En";
                    current_block_records.back().data.resize((*out_tmp)["En"].GetCount());
                        {
                        auto it1=(*out_tmp)["En"].GetData().begin();
                        for(auto it=current_block_records.back().data.begin();it!=current_block_records.back().data.end();++it)
                            {
                            *it=*it1;
                            ++it1;
                            }
                        }
                    break;
                    }
                }
            }
    
    public:
        plot_processor(std::string output_dir):_output_dir(output_dir)
            {
            current_id=0;
            a.params()["gam"].SetValue(1.0);
            a.params()["eps"].SetValue(10.0);
            a.params()["nt"].SetValue(5.001);
            a.params()["NoiseWidth"].SetValue(1e-11);
            a.params()["Q"].SetValue(5000);
            a.params()["teta"].SetValue(000.00);
            a.params()["dt"].SetValue(0.00001);
            a.params()["Nmax"].SetValue(10000000.0);
            a.params()["dN"].SetValue(10.0);
            std::ofstream ofs;
            ofs.open(output_dir+"data.bin",std::ofstream::out | std::ofstream::trunc);
            ofs.close();
            ofs.open(output_dir+"data.ini",std::ofstream::out | std::ofstream::trunc);
            ofs.close();
            
            my_cmd.add_variable(a.params());
            my_cmd.add_command("plot",std::bind(&plot_processor::plot,this,std::placeholders::_1));
            my_cmd.add_command("clear",std::bind(&plot_processor::clear,this,std::placeholders::_1));
            my_cmd.add_command("save",std::bind(&plot_processor::save,this,std::placeholders::_1));
            my_cmd.run();
            }
        ~plot_processor()
            {
            }
    };




int main(int argc,char ** argv)
    {
    plot_processor pp("./res/plot1/");    
    return 0;
    }
