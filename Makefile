all: 
	mkdir -p ./build
	mkdir -p ./bin
	cmake -D CMAKE_BUILD_TYPE=Release -B./build -H./src
	make -C ./build -j `nproc` VERBOSE=1
	./bin/ParametricOsc
reader:
	mkdir -p ./build
	mkdir -p ./build/reader
	mkdir -p ./bin
	mkdir -p ./bin/reader
	cmake -D CMAKE_BUILD_TYPE=Release -B./build/reader -H./src/reader
	make -C ./build/reader -j `nproc` VERBOSE=1
	./bin/reader/reader
ploter1:ploter1_build ploter1_run 
ploter1_build:	
	mkdir -p ./build
	mkdir -p ./build/ploter1
	mkdir -p ./bin
	mkdir -p ./bin/ploter1
	cmake -D CMAKE_BUILD_TYPE=Release -B./build/ploter1 -H./src/ploter1
	make -C ./build/ploter1 -j `nproc` VERBOSE=1
ploter1_run:	
	./bin/ploter1/ploter1
ploter2:ploter2_build ploter2_run
ploter2_build:	
	mkdir -p ./build
	mkdir -p ./build/ploter2
	mkdir -p ./bin
	mkdir -p ./bin/ploter2
	cmake -D CMAKE_BUILD_TYPE=Release -B./build/ploter2 -H./src/ploter2
	make -C ./build/ploter2 -j `nproc` VERBOSE=1
ploter2_run:	
	./bin/ploter2/ploter2
ploter3:ploter3_build ploter3_run
ploter3_build:	
	mkdir -p ./build
	mkdir -p ./build/ploter3
	mkdir -p ./bin
	mkdir -p ./bin/ploter3
	cmake -D CMAKE_BUILD_TYPE=Release -B./build/ploter3 -H./src/ploter3
	make -C ./build/ploter3 -j `nproc` VERBOSE=1
ploter3_run:	
	./bin/ploter3/ploter3
